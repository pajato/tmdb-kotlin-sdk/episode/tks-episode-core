package com.pajato.tks.episode.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public class Crew(
    public val adult: Boolean = false,
    @SerialName("credit_id")
    public val creditId: String = "",
    public val gender: Int = 0,
    public val department: String = "",
    @SerialName("episode_count")
    public val episodeCount: Int = -1,
    @SerialName("known_for_department")
    public val knownForDepartment: String = "",
    public val id: Int = 0,
    public val job: String = "",
    public val name: String = "",
    @SerialName("original_name")
    public val originalName: String = "",
    @SerialName("person_id")
    public val personId: String = "",
    public val popularity: Double = 0.0,
    @SerialName("profile_path")
    public val profilePath: String = "",
)
