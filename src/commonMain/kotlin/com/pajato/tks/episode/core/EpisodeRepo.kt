package com.pajato.tks.episode.core

import com.pajato.tks.common.core.EpisodeKey

public interface EpisodeRepo {
    public suspend fun getEpisode(key: EpisodeKey): Episode
}
