package com.pajato.tks.episode.core

import com.pajato.tks.common.core.TmdbId
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Episode(
    @SerialName("air_date")
    val airDate: String = "",
    val crew: List<Crew> = listOf(),
    @SerialName("episode_number")
    val episodeNumber: Int = -1,
    @SerialName("guest_stars")
    val guestStars: List<GuestStars> = listOf(),
    val name: String = "",
    val overview: String = "",
    val id: TmdbId = -1,
    @SerialName("show_id")
    val showId: TmdbId = -1,
    @SerialName("production_code")
    val productionCode: String = "",
    @SerialName("season_number")
    val seasonNumber: Int = -1,
    @SerialName("still_path")
    val stillPath: String = "",
    @SerialName("vote_average")
    val voteAverage: Double = 0.0,
    @SerialName("vote_count")
    val voteCount: Int = 0,
    val runtime: Int = 0,
)
