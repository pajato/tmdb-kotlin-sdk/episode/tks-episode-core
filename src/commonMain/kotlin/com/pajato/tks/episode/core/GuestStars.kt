package com.pajato.tks.episode.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("guest_stars")
public data class GuestStars(
    val adult: Boolean = false,
    @SerialName("credit_id")
    val creditId: String = "",
    val department: String = "",
    @SerialName("episode_count")
    val episodeCount: Int = -1,
    val gender: Int = 0,
    val id: Int = -1,
    val job: String = "",
    @SerialName("known_for_department")
    val knownForDepartment: String = "",
    val name: String = "",
    @SerialName("original_name")
    val originalName: String = "",
    @SerialName("person_id")
    val personId: String = "",
    val popularity: Double = 0.0,
    @SerialName("profile_path")
    val profilePath: String = "",
    val character: String = "",
    val order: Int = -1,
)
