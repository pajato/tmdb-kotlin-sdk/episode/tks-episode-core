package com.pajato.tks.episode.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class CrewUnitTest : ReportingTestProfiler() {
    @Test fun `When a test crew object is serialized and deserialized, verify behavior`() {
        val crew = Crew()
        val json = jsonFormat.encodeToString(crew)
        assertEquals("{}", json)
        assertEquals(0, jsonFormat.decodeFromString<Crew>(json).id)
    }
}
