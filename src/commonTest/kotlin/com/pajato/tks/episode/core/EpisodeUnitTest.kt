package com.pajato.tks.episode.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class EpisodeUnitTest : ReportingTestProfiler() {
    @Test fun `When a test episode object is serialized and deserialized, verify`() {
        val episode = Episode()
        val json = jsonFormat.encodeToString(episode)
        assertEquals("{}", json)
        assertEquals(-1, jsonFormat.decodeFromString<Episode>(json).id)
        assertTrue(jsonFormat.decodeFromString<Episode>(json).crew.isEmpty())
    }
}
